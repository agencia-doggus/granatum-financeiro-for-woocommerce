<?php

defined('ABSPATH') or die;

if(!class_exists('WC_Granatum_Gateway')) {

	class WC_Granatum_Gateway extends WC_Payment_Gateway {

		private $granatum_api = null;

		public function __construct() {

			$this->id = 'granatum_gateway';
			$this->method_title = 'Granatum Gateway';
			$this->method_description = 'Realizar pagamentos com boleto e cartão de crédito via Granatum Financeiro';

			$this->has_fields = true;

			$this->init_form_fields();
			$this->init_settings();

			$this->enabled = $this->get_option( 'enabled' );
			$this->title = $this->get_option( 'title' );
			$this->description = $this->get_option( 'description' );

			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		}

		public function init_form_fields() {

			$this->form_fields = array(
				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'woocommerce' ),
					'type'    => 'checkbox',
					'label'   => 'Habilitar Granatum Gateway',
					'default' => 'no',
				),
				'title' => array(
					'title'       => __( 'Title', 'woocommerce' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
					'default'     => 'Granatum Financeiro',
					'desc_tip'    => true,
				),
				'description' => array(
					'title'       => __( 'Description', 'woocommerce' ),
					'type'        => 'textarea',
					'desc_tip'    => true,
					'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
					'default'     => 'Pagamento com cartão de crédito ou boleto via Granatum Financeiro.',
				),

				'options' => array(
					'title'       => 'Opções do Granatum Financeiro',
					'type'        => 'title',
					'description' => 'Escolha como utilizar os pagamentos via Granatum Financeiro.',
				),
				'creditcard_enabled' => array(
					'title'   => 'Habilitar pagamento por cartão de crédito',
					'type'    => 'checkbox',
					'label'   => 'Habilitar pagamento por cartão de crédito',
					'default' => 'yes',
				),
				'billet_enabled' => array(
					'title'   => 'Habilitar pagamento por boleto bancário',
					'type'    => 'checkbox',
					'label'   => 'Habilitar pagamento por boleto bancário',
					'default' => 'yes',
				),
				'installment_enabled' => array(
					'title'   => 'Habilitar parcelamento por boleto',
					'type'    => 'checkbox',
					'label'   => 'Habilitar parcelamento por boleto',
					'default' => 'no',
				),
				'installment_times' => array(
					'title'   => 'Limite de parcelamento por boleto',
					'type'    => 'text',
					'label'   => 'Limite de parcelamento por boleto',
					'default' => 1,
				),

				'api_details' => array(
					'title'       => 'Credenciais da API do Granatum Financeiro',
					'type'        => 'title',
					'description' => 'Insira as informações necessárias para realizar a comunicação com a API do Granatum Financeiro.',
				),
				'api_key' => array(
					'title'       => 'Token da API',
					'type'        => 'text',
					'description' => 'Token da API disponível em "Configurações > Minha Empresa"',
					'desc_tip'    => true,
				),
				'api_account_id' => $this->bank_accounts_settings(),
				'api_category_id' => $this->product_category_settings(),
			);
		}

		public function payment_fields() {

			$description = $this->get_description();
			if ( $description ) {
				echo wpautop( wptexturize( $description ) ); // @codingStandardsIgnoreLine.
			}

			$creditcard_enabled = $this->get_option('creditcard_enabled');
			$billet_enabled = $this->get_option('billet_enabled');
			$installment_enabled = $this->get_option('installment_enabled');
			$installment_times = $this->get_option('installment_times');

			?>
			<div id="wc-granatum-gateway-fields">
				<?php if($creditcard_enabled && $billet_enabled): ?>
					<div>
						<label>Escolha a forma de pagamento: </label>
						<br>
						<select id="wc-granatum-gateway-method" name="wc-granatum-gateway-method">
							<option value="cartao_credito">Cartão de crédito</option>
							<option value="boleto">Boleto bancário</option>
						</select>
					</div>
				<?php endif; ?>
				<?php if($billet_enabled && $installment_enabled && $installment_times > 1): ?>
					<div id="wc-granatum-gateway-installment-options" <?php echo ($creditcard_enabled && $billet_enabled) ? 'style="display:none;"':''; ?>>
						<label>Escolha o número de parcelas:</label>
						<br>
						<select id="wc-granatum-gateway-installment" name="wc-granatum-gateway-installment">
							<?php $this->the_installment_options(); ?>
						</select>
					</div>
				<?php endif; ?>
			</div>
			<?php if($creditcard_enabled && $billet_enabled && $installment_enabled && $installment_times > 1): ?>
				<script type="text/javascript">
					jQuery(function() {
						jQuery('#wc-granatum-gateway-method').on('change', function() {
							isBilletMethod = jQuery(this).val() == 'boleto';
							if(isBilletMethod) {
								jQuery('#wc-granatum-gateway-installment-options').show();
							} else {
								jQuery('#wc-granatum-gateway-installment-options').hide();
							}
						});
					});
				</script>
			<?php endif; ?>
			<?php
		}

		private function the_installment_options() {

			$installment_times = $this->get_option('installment_times');

			$total = wc_prices_include_tax() ? WC()->cart->get_cart_contents_total() + WC()->cart->get_cart_contents_tax() : WC()->cart->get_cart_contents_total();

			for($i = 1; $i <= $installment_times; $i++) {

				echo sprintf('<option value="%d">%dx de %s sem juros</option>',
					$i,
					$i,
					wc_price( $total / $i )
				);
			}
		}

		public function process_payment($order_id) {

			global $woocommerce;
		    $order = new WC_Order($order_id);

		    $api_key = $this->get_option('api_key');
		    $this->granatum_api = new Granatum_API($api_key);

		    $granatum_client = $this->get_granatum_client($order);
		    if(empty($granatum_client)) {
		    	return;
		    }

		    $granatum_order = $this->get_granatum_order($order, $granatum_client);
		    if(empty($granatum_order)) {
		    	wc_add_notice( 'Erro [7]: Não foi possível gerar a cobrança no Granatum.', 'error' );
				return;
		    }

		    $granatum_charge = $this->granatum_api->create_charge($granatum_order);
		    if(is_wp_error($granatum_charge)) {

		    	wc_add_notice( 'Erro [3]: Não foi possível gerar a cobrança no Granatum.', 'error' );
				return;
		    }

		    $order->update_status('on-hold', 'Granatum: cliente gerou uma cobrança. Aguardando confirmação de pagamento. Status do pedido alterado para Aguardando.');

		    $woocommerce->cart->empty_cart();

		    return array(
		        'result' => 'success',
		        'redirect' => $this->get_return_url($order),
		    );
		}

		/**
		 * Return whether or not this gateway still requires setup to function.
		 *
		 * When this gateway is toggled on via AJAX, if this returns true a
		 * redirect will occur to the settings page instead.
		 *
		 * @since 3.4.0
		 * @return bool
		 */
		public function needs_setup() {

			return true;
		}

		/**
		 * Check if the gateway is available for use.
		 *
		 * @return bool
		 */
		public function is_available() {

			$api_key = $this->get_option('api_key');
		    $account_id = $this->get_option('api_account_id');
		    $category_id = $this->get_option('api_category_id');

		    $creditcard_enabled = $this->get_option('creditcard_enabled');
			$billet_enabled = $this->get_option('billet_enabled');

		    return parent::is_available()
		    	&& !empty($api_key)
		    	&& !empty($account_id)
		    	&& !empty($category_id)
		    	&& ( $creditcard_enabled || $billet_enabled );
		}

		private function get_payment_method() {

			$method = empty($_POST['wc-granatum-gateway-method']) ? '' : sanitize_text_field($_POST['wc-granatum-gateway-method']);
			if(!in_array($method, array('boleto', 'cartao_credito'))) {

				$method = '';
			}

			return $method;
		}

		private function get_granatum_order($order, $granatum_client) {

			$granatum_order = array(
				'conta_id' => $this->get_option('api_account_id'),
				'cliente_id' => $granatum_client->id,
				'data_vencimento' => $this->get_next_weekday(),
				'email' => $granatum_client->email,
				'tipo_emissao' => 1,
				'itens' => array(),
			);

			$category_id = $this->get_option('api_category_id');

			foreach($order->get_items() as $item_id => $item ) {

				$granatum_order['itens'][] = array(
					'descricao' => $item->get_name(),
					'valor'	=> number_format($item->get_total(), 2, '.', ''),
					'categoria_id' => $category_id,
				);
			}

			$payment_method = $this->get_payment_method();
			if(empty($payment_method)) {
				wc_add_notice( 'Erro [6]: Forma de pagamento inválida.', 'error' );
				return '';
			}
			$granatum_order['tipo_cobranca'] = $payment_method;

			$granatum_order = $this->configure_installments($granatum_order);

			return $granatum_order;
		}

		private function configure_installments($granatum_order) {

			if($granatum_order['tipo_cobranca'] == 'boleto') {

				$installment_enabled = $this->get_option('installment_enabled');
				$installment_times = $this->get_option('installment_times');

				if($installment_enabled && $installment_times > 1) {

					$installments = empty($_POST['wc-granatum-gateway-installment']) ? 1 : intval($_POST['wc-granatum-gateway-installment']);

					if($installments > 1) {

						$installments = min($installments, $installment_times);

						$granatum_order['periodicidade'] = 'M1';
						$granatum_order['parcelas'] = $installments;

						for($i = 0; $i < count($granatum_order['itens']); $i++) {

							$granatum_order['itens'][$i]['descricao'] .= sprintf(' (Parcelado em %d vezes)', $installments);
							$granatum_order['itens'][$i]['valor'] = round( $granatum_order['itens'][$i]['valor'] / $installments, 2 );
						}
					}
				}
			}

			return $granatum_order;
		}

		private function get_granatum_client($order) {

			$cpf_cnpj = preg_replace("/[^0-9]/", '', $order->get_meta('_billing_cpf'));
		    if(empty($cpf_cnpj)) {
		    	$cpf_cnpj = preg_replace('/[^0-9]/', '', $order->get_meta('_billing_cnpj'));
		    }

		    if(empty($cpf_cnpj)) {
		    	wc_add_notice( 'Erro: O CPF ou CNPJ deve ser fornecido.', 'error' );
				return;
		    }

		    $clients = $this->granatum_api->list_clients(array(
		    	'documento' => $cpf_cnpj,
		    ));
		    if(is_wp_error($clients)) {
		    	wc_add_notice( 'Erro: Não foi possível se comunicar com o Granatum Financeiro.', 'error' );
				return;
		    }

		    $default_client = $this->get_default_client_info($cpf_cnpj, $order);
		    if(empty($default_client)) {
		    	return;
		    }

		    if(empty($clients)) {

		    	$default_client['observacao'] = 'Cliente cadastrado via WooCommerce';

		    	$client = $this->granatum_api->create_client($default_client);
		    	if(is_wp_error($client)) {
		    		wc_add_notice( 'Erro [2]: Não foi possível cadastrar o cliente no Granatum Financeiro.', 'error' );
					return;
		    	}

		    	$clients = array($client);

		    } else {

		    	$client = $clients[0];
		    	$missing_client_info = array();

		    	foreach($default_client as $key => $value) {

		    		if(empty($client->$key)) {

		    			$missing_client_info[$key] = $value;
		    		}
		    	}

		    	if(!empty($missing_client_info)) {

		    		$missing_client_info['id'] = $client->id;
		    		$missing_client_info['nome'] = $client->nome;
		    		$missing_client_info['cep'] = '91770-641';

		    		$client = $this->granatum_api->update_client($missing_client_info);
			    	if(is_wp_error($client)) {
			    		wc_add_notice( 'Erro [4]: Não foi possível atualizar o cadastro do cliente no Granatum Financeiro.', 'error' );
						return;
			    	}

			    	$clients[0] = $client;
		    	}
		    }

		    return $clients[0];
		}

		private function get_default_client_info($cpf_cnpj, $order) {

			$state_id = $this->get_state_id($order->get_billing_state());
	    	if(empty($state_id)) {
	    		wc_add_notice( 'Erro: Estado não encontrado. Verifique o campo e tente novamente.', 'error' );
				return;
	    	}

	    	$city_id = $this->get_city_id($order->get_billing_city(), $state_id);
	    	if(empty($city_id)) {
	    		wc_add_notice( 'Erro: Cidade não encontrada. Verifique o campo e tente novamente.', 'error' );
				return;
	    	}

	    	return array(
	    		'nome' => $order->get_billing_first_name().' '.$order->get_billing_last_name(),
				'documento' => $cpf_cnpj,
				'telefone' => preg_replace('/[^0-9]/', '', $order->get_billing_phone()),
				'email' => $order->get_billing_email(),
				'endereco' => $order->get_billing_address_1(),
				'endereco_numero' => $order->get_meta('_billing_number'),
				'endereco_complemento' => $order->get_billing_address_2(),
				'bairro' => $order->get_meta( '_billing_neighborhood' ),
				'cep' => $order->get_billing_postcode(),
				'cidade_id' => $city_id,
				'estado_id' => $state_id,
			);
		}

		private function get_city_id($city, $state_id) {

			$city_name = strtolower(iconv('utf8', 'ASCII//TRANSLIT', $city));

			$granatum_cities_of_state = $this->get_granatum_cities_of_state($state_id);

			foreach($granatum_cities_of_state as $granatum_city) {

				$granatum_city_name = strtolower(iconv('utf8', 'ASCII//TRANSLIT', $granatum_city->nome));

				if($granatum_city_name == $city_name) {

					return $granatum_city->id;
				}
			}

			return '';
		}

		private function get_state_id($state) {

			$granatum_states = $this->get_granatum_states();

			foreach($granatum_states as $granatum_state) {

				if(strcasecmp($granatum_state->sigla, $state) == 0) {

					return $granatum_state->id;
				}
			}

			return '';
		}

		private function get_granatum_cities_of_state($state_id) {

			$granatum_cities = get_option('granatum_api_cities', array());

			if(!isset($granatum_cities[$state_id])) {

				$granatum_cities_of_state = $this->granatum_api->list_cities($state_id);

				if(!is_wp_error($granatum_cities_of_state)) {

					$granatum_cities[$state_id] = $granatum_cities_of_state;
					update_option('granatum_api_cities', $granatum_cities);
				}
			}

			$granatum_cities_of_state = isset($granatum_cities[$state_id]) ? $granatum_cities[$state_id] : array();

			return $granatum_cities_of_state;
		}

		private function get_granatum_states() {

			$granatum_states = get_option('granatum_api_states', false);

			if(!$granatum_states) {

				$granatum_states = $this->granatum_api->list_states();

				if(!is_wp_error($granatum_states)) {

					update_option('granatum_api_states', $granatum_states);
				}
			}

			return $granatum_states;
		}

		private function bank_accounts_settings() {

			$bank_account_settings = array(
				'title'       => 'Identificador da Conta Bancária',
				'type'        => 'text',
				'description' => 'Token da API disponível em "Configurações > Minha Empresa"',
				'desc_tip'    => true,
			);

			$api_key = $this->get_option('api_key');
			if($api_key) {

				$this->granatum_api = new Granatum_API($api_key);

				$bank_accounts = $this->granatum_api->list_bank_accounts();
				if(!is_wp_error($bank_accounts)) {

					$options = array(
						0 => 'Selecione uma conta bancária',
					);
					foreach($bank_accounts as $bank_account) {

						$options[$bank_account->id] = $bank_account->descricao;
					}

					$bank_account_settings['type'] = 'select';
					$bank_account_settings['class'] = 'wc-enhanced-select';
					$bank_account_settings['options'] = $options;
				}
			}

			return $bank_account_settings;
		}

		private function product_category_settings() {

			$bank_account_settings = array(
				'title'       => 'Categoria padrão dos produtos vendidos',
				'type'        => 'text',
				'description' => 'Selecione em qual categoria os produtos vendidos devem ser registrados no painel do Granatum Financeiro',
				'desc_tip'    => true,
			);

			$api_key = $this->get_option('api_key');
			if($api_key) {

				$this->granatum_api = new Granatum_API($api_key);

				$categories = $this->granatum_api->list_categories();
				if(!is_wp_error($categories)) {

					$options = array(
						0 => 'Selecione uma categoria',
					);
					$options = array_replace($options, $this->extract_child_categories($categories));

					$bank_account_settings['type'] = 'select';
					$bank_account_settings['class'] = 'wc-enhanced-select';
					$bank_account_settings['options'] = $options;
				}
			}

			return $bank_account_settings;
		}

		private function extract_child_categories($categories) {

			$options = array();

			foreach($categories as $category) {

				if(empty($category->categorias_filhas)) {

					$options[$category->id] = $category->descricao;

				} else {

					$options = array_replace($options, $this->extract_child_categories($category->categorias_filhas));
				}
			}

			return $options;
		}

		private function get_next_weekday() {

			$next_weekday = new DateTime();
			$next_weekday->modify('+1 weekday');

			return $next_weekday->format('Y-m-d');
		}
	}
}

function add_granatum_gateway_class($methods) {

	$methods[] = 'WC_Granatum_Gateway';
    return $methods;
}
add_filter('woocommerce_payment_gateways', 'add_granatum_gateway_class');
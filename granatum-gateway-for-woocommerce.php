<?php
/**
 * Plugin Name: Granatum Gateway for WooCommerce
 * Plugin URI:  https://agenciadoggus.com.br
 * Description: Adds the Granatum Credit Card and Granatum Billet payment methods
 * Author:      Agência Doggus
 * Author URI:  https://agenciadoggus.com.br
 * Version:     0.1
 * License:     GPLv2 or later
 * Text Domain: granatum-gateway-for-woocommerce
 * WC requires at least: 3.0
 * WC tested up to: 4.4
 *
 * @package Granatum_Gateway_For_WooCommerce
 */

if (!defined('ABSPATH')) {

	exit; // Exit if accessed directly.
}

if(!class_exists('Granatum_Gateway_For_WooCommerce')) {

	class Granatum_Gateway_For_WooCommerce {

		const VERSION = '0.1';

		protected static $instance = null;

		private function __construct() {

			if(class_exists('WooCommerce') && class_exists('Extra_Checkout_Fields_For_Brazil')) {

				$this->includes();
				$this->filters();

			} else {

				add_action('admin_notices', array($this, 'woocommerce_fallback_notice'));
			}
		}

		public static function get_instance() {

			// If the single instance hasn't been set, set it now.
			if ( null === self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		private function includes() {

			include_once dirname( __FILE__ ) . '/class-granatum-gateway.php';
			include_once dirname( __FILE__ ) . '/class-granatum-api.php';
		}

		private function filters() {

			add_filter('woocommerce_billing_fields', array($this, 'configure_required_checkout_fields'), 20, 1);
		}

		public function configure_required_checkout_fields($fields) {

			$fields['billing_neighborhood']['required'] = true;

			return $fields;
		}

		/**
		 * WooCommerce fallback notice.
		 *
		 * @return string Fallack notice.
		 */
		public function woocommerce_fallback_notice() {

			if(!class_exists('WooCommerce')) {

				echo '<div class="error"><p>Granatum Gateway for WooCommerce depende do plugin <a href="http://wordpress.org/extend/plugins/woocommerce/" target="_blank">WooCommerce</a> para funcionar.</p></div>';
			}

			if(!class_exists('Extra_Checkout_Fields_For_Brazil')) {

				echo '<div class="error"><p>Granatum Gateway for WooCommerce depende do plugin <a href="https://wordpress.org/plugins/woocommerce-extra-checkout-fields-for-brazil/" target="_blank">Brazilian Market on WooCommerce</a> para funcionar.</p></div>';
			}
		}
	}

	add_action('plugins_loaded', array( 'Granatum_Gateway_For_WooCommerce', 'get_instance'));
}
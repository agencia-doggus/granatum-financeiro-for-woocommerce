<?php

defined('ABSPATH') or die;

if(!class_exists('Granatum_API')) {

	class Granatum_API {

		private const URL = 'https://api.granatum.com.br/v1/';
		private const HEADERS = array(
			'headers' => array(
				'Content-Type' => 'application/x-www-form-urlencoded'
			),
		);
		private const SUCCESSFULL_HTTP_CODES = array(200, 201);

		protected $api_key = '';

		public function __construct($api_key) {

			$this->api_key = $api_key;
		}

		public function create_client($args) {

			$default = array(
				'nome' => '', //	Nome/Razão Social do cliente	String	Requerido
				'documento' => '', //	CPF/CNPJ do cliente	String	Opcional
				'email' => '', //	Email do cliente	String	Opcional
				'endereco' => '', //	Apenas logradouro. Ex: Rua 13 de maio.	String	Opcional
				'endereco_numero' => '', //	Número do endereço	String	Opcional
				'bairro' => '', //	Bairro do endereço	String	Opcional
				'cep' => '', //	Código postal do endereço	String	Opcional
			);

			$args = array_merge($default, $args);

			$required_fields = array('nome', 'email', 'documento', 'endereco', 'endereco_numero', 'bairro', 'cep');
			foreach ($required_fields as $required_field) {
				if(empty($args[$required_field])) {
					return new WP_Error('error', 'O campo "'.$required_field.'" não foi preenchido.');
				}
			}

			return $this->remote_post('clientes', $args);
		}

		public function create_charge($args) {

			$default = array(
				'conta_id' => '',
				'cliente_id' => '',
				'data_vencimento' => '',
				'tipo_cobranca' => '',
				'itens' => '',
				'email' => '',
				'tipo_emissao' => '', // 1 - Emitir imediatamente, 2 - Agendar emissão
				// 'periodicidade' => '', // D7 (Semanal), D15 (Quinzenal), M1 (Mensal), M2 (Bimestral), M3 (Trimestral), M6 (Semestral), M12 (Anual)
				// 'parcelas' => '',
			);

			$args = array_merge($default, $args);

			$required_fields = array('conta_id', 'cliente_id', 'data_vencimento', 'tipo_cobranca', 'email', 'itens', 'tipo_emissao');
			foreach ($required_fields as $required_field) {
				if(empty($args[$required_field])) {
					return new WP_Error('error', 'O campo "'.$required_field.'" não foi preenchido.');
				}
			}

			$args = http_build_query($args, null, ini_get('arg_separator.output'), PHP_QUERY_RFC1738);
			$args = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $args);

			return $this->remote_post('cobrancas', $args);
		}

		public function update_client($args) {

			$default = array(
				'id' => '',
				'nome' => '',
			);

			$args = array_merge($default, $args);

			if(empty($args['id'])) {
				return new WP_Error('error', 'O campo "id" não foi preenchido.');
			}
			if(empty($args['nome'])) {
				return new WP_Error('error', 'O campo "nome" não foi preenchido.');
			}

			return $this->remote_put('clientes/'.$args['id'], $args);
		}

		public function list_clients($args) {

			return $this->remote_get('clientes', '', $args);
		}

		public function list_bank_accounts() {

			return $this->remote_get('contas');
		}

		public function list_categories() {

			return $this->remote_get('categorias');
		}

		public function list_cities($state_id) {

			return $this->remote_get('cidades', 'estado_id='.$state_id);
		}

		public function list_states() {

			return $this->remote_get('estados');
		}

		private function remote_put($route, $args) {

			return $this->remote_request('PUT', $route, '', $args);
		}

		private function remote_post($route, $args) {

			return $this->remote_request('POST', $route, '', $args);
		}

		private function remote_get($route, $query_args = '', $args = array()) {

			return $this->remote_request('GET', $route, $query_args, $args);
		}

		private function remote_request($method, $route, $query_args = '', $args = array()) {

			$response = wp_remote_request(Granatum_API::make_route($route, $query_args), array(
				Granatum_API::HEADERS,
				'body' => $args,
				'method' => $method,
				'timeout' => 15,
			));

			if(is_wp_error($response)) {
				error_log(var_export($response, true));
				return $response;
			}

			if(!in_array($response['response']['code'], Granatum_API::SUCCESSFULL_HTTP_CODES)) {

				error_log(var_export($response, true));

				return new WP_Error('error', 'Erro '.$response['response']['code'].' na comunicação com o Granatum Financeiro.');
			}

			return json_decode($response['body']);
		}

		private function make_route($route, $query_args = '') {

			return Granatum_API::URL.$route.'?access_token='.$this->api_key.($query_args ? '&'.$query_args : '');
		}
	}
}
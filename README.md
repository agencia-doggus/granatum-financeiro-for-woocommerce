# Granatum Gateway for WooCommerce

[![Doggus](http://agenciadoggus.com.br/dev-doggus.png)](https://agenciadoggus.com.br)

*Este plugin está em modo beta e ainda não deve ser utilizado em ambiente de produção.*

Este plugin adiciona o Granatum Financeiro como forma de pagamento no WooCommerce.

  - Pagamento via Cartão de Crédito
  - Pagamento via boleto bancário
  - Parcelamento via boleto bancário

*Importante:* Este plugin foi desenvolvido de forma independente e não possui nenhuma relação com a empresa Granatum Financeiro.

## Dependências
Este plugin depende dos seguintes plugins para funcionar:
- [WooCommerce](https://wordpress.org/plugins/woocommerce/) by Automattic.
- [WooCommerce Extra Checkout Fields for Brazil](https://wordpress.org/plugins/woocommerce-extra-checkout-fields-for-brazil/) by Claudio Sanches.

## Bugs ou problemas de integração
Bugs podem ser registrados no Gitlab do projeto.